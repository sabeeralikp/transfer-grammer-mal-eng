#import packages
from transformers import pipeline
from mlmorph import Analyser
import string
import re
from ai4bharat.transliteration import XlitEngine
from func import *
from data_read import *

#remove punctuations
def remove_punctuation(text):

  punctuation_chars = string.punctuation
  translator = str.maketrans('', '', punctuation_chars)
  return text.translate(translator)


#load model for pos tagging and ner identification
pipe = pipeline('token-classification',"bert_malayalam_pos_tagger")
analyser = Analyser()
ner_pipe = pipeline("token-classification", "IndicNER")

# Initialize a transliteration engine
e=XlitEngine(src_script_type="indic",beam_width=10,rescore=False)


#function to word pos tagging and name entity identification
def word_feature_extractor(sentence):
    representation_list=[]
    words = remove_punctuation(sentence).split()
    for word in words:
        an = analyser.analyse(word)
        if(len(an) != 0):
          word_list = re.findall(r'[\u0D00-\u0D7F]+', str(an[0]))
          for wl in word_list:
            res = pipe(wl)
            ner = ner_pipe(wl)
            if(len(ner) == 0):
              representation_list.append({'root_word': word,'word': wl,'entity':res[0]['entity'], 'morph': an[0]})
              print(representation_list)
              continue
            
            representation_list.append({'root_word': word,'word': wl,'entity':res[0]['entity'], 'ner': ner[0]['entity'],  'morph': an[0]})

    return representation_list

def translator(sentence):

  translation = ''
  ner_store=''

  #call function to 
  representation_list=word_feature_extractor(sentence)


  #two word translation
  if (len(representation_list) == 2) or check_two_word_conjuction(representation_list):

    translation = ' '
    for lst in representation_list:

      if (lst['entity']=='N_NNP' or lst['entity']=='N_NN') and 'ner' not in lst:


        if '<n>' in tuple(lst['morph'])[0]:           
          translation = noun_presence(lst)
        else:
          translation+=noun.get(lst['word'])+' '
        
        translation+=prepositions_check(lst)

      elif lst['entity'] == "PR_PRP":
        translation += proper_pronoun(lst,translation)

      elif 'ner' in lst:
        if lst['word'] not in noun:

          translation += name_entity(lst, e)
        else:
          translation += noun.get(lst['word'])+' '

        translation += prepositions_check(lst)

      elif lst['entity'] == 'DM_DMD' and 'ner' not in lst:
        translation = demonstrative.get(lst['word'])+' '

      elif lst['entity'] == 'V_VM_VINF' or '<v>' in tuple(lst['morph'])[0]:

        if '<present>' in tuple(lst['morph'])[0]:
          translation = simple_present(lst,translation)

        elif '<past>' in tuple(lst['morph'])[0]:
          translation = simple_past(lst,translation)

        elif '<future>' in tuple(lst['morph'])[0]:
          translation=simple_future(lst,translation)

        elif '<iterative-present-aspect>' in tuple(lst['morph'])[0]:
          translation=present_continous(lst,translation)

        elif lst['word'] in past_conjuntions:
          translation=past_continous(lst,translation)

        elif '<remote-perfect-aspect>' in tuple(lst['morph'])[0]:
          
          if lst['word'] in conjunctions:
            continue

          translation+='have' if translation.strip() in present_auxilary_inflection.keys() else 'has'
          translation+=' '
          translation+=verb_3.get(lst['word'])

        elif '<simple-perfect-aspect>' in tuple(lst['morph'])[0]:
          translation+=past_perfect(lst)

        elif '<aff>' in tuple(lst['morph'])[0] and lst['entity']=='V_VN_VINF':
          translation+=personal_pronoun(lst)
        
        elif '<promissive-mood>' in tuple(lst['morph'])[0]:
          translation+=verb_1.get(lst['word'])

    return translation, representation_list

  #three word translation
  elif (len(representation_list)==3):

    #TODO: Check wheather the words are in the form (Subject + Object + Verb)
    # replacing verb and object
    representation_list[1], representation_list[2] = representation_list[2], representation_list[1]

    translation=' '

    for lst in representation_list:

      if (lst['entity']=='N_NNP' or lst['entity']=='N_NN') and 'ner' not in lst:


        if '<n>' in tuple(lst['morph'])[0]:
          translation+=noun_presence(lst)

        else:
          translation+=noun.get(lst['word'])+' '
        
        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '

      elif lst['entity']=="PR_PRP":
        translation=proper_pronoun(lst,translation)
        print(translation)

      elif lst['entity']=='DM_DMD' and 'ner' not in lst:
        translation=demonstrative.get(lst['word'])+' '

      #name transileration
      elif 'ner' in lst :
        ner_store='ner'
        if lst['word'] not in noun:
          out = e.translit_word(lst['word'],lang_code="ml",topk=1)
          translation+=str(out[0])+' '
          print(translation)
        else:
          translation+=noun.get(lst['word'])+' '

        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '

      elif lst['entity']=='V_VM_VINF' or '<v>' in tuple(lst['morph'])[0]:
        
        #simple present
        if '<present>' in tuple(lst['morph'])[0]:
          translation = simple_present(lst,translation,ner_store)
          #translation+=verb_1.get(lst['word'])+'s'   

        #simple past
        elif '<past>' in tuple(lst['morph'])[0]:
          translation=simple_past(lst,translation)

        #simple future
        elif '<future>' in tuple(lst['morph'])[0]:
          translation=simple_future(lst,translation)  

        # Present continuous
        elif '<iterative-present-aspect>' in tuple(lst['morph'])[0]:
          translation=present_continous(lst,translation)     

        #past continuous
        elif lst['word'] in past_conjuntions:
          translation=past_continous(lst,translation) 

        # Present Perfect
        elif '<remote-perfect-aspect>' in tuple(lst['morph'])[0]:  

          if lst['word'] in conjunctions:
            continue
          translation+='have' if translation.strip() in present_auxilary_inflection.keys() else 'has'
          translation+=' '
          translation+=verb_3.get(lst['word'])

        # Past Perfect
        elif '<simple-perfect-aspect>' in tuple(lst['morph'])[0]:
          translation+=past_perfect(lst)
        
        else:
          print("Rules Breaked")
        
        # Additional Phrasal Verb
        if '<aff>' in tuple(lst['morph'])[0]:
          translation += personal_pronoun(lst)

        elif '<accusative>' in tuple(representation_list[1]['morph'])[0]:
          if(representation_list[1]['root_word'].endswith('യെ') and representation_list[1]['root_word'] in ["നോക്കി"]):
            translation += "at"
            translation += " "
      
    return translation, representation_list

  elif (len(representation_list)==4):
    translation=' '
    word_type={}
    for lst in representation_list:

      word_type.update({lst['word']:lst['entity']})

      if (lst['entity']=='N_NNP' or lst['entity']=='N_NN') and 'ner' not in lst:


        if '<n>' in tuple(lst['morph'])[0] and '<aff>' not in tuple(lst['morph'])[0]:
          if '<genitive>' in tuple(lst['morph'])[0]:
            translation+=noun.get(lst['word'])+"'s"
            translation+=' '
          else:
            translation+=noun_presence(lst)

        elif '<aff>' in tuple(lst['morph'])[0]:
          if lst['word'] in affix:
            translation+='is'
            translation+=' '

        else:
          translation+=noun.get(lst['word'])+' '
        
        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '
        print(translation)

      elif lst['entity']=='CC_CCD':
        if 'and' not in translation:
          translation+='and'
          translation+=' '

      elif lst['entity']=="PR_PRP":
        translation=proper_pronoun(lst,translation)

      elif lst['entity']=='DM_DMD' and 'ner' not in lst:
        translation=demonstrative.get(lst['word'])+' '
      
      elif lst['entity']=='JJ':
        translation+=noun.get(lst['word'])
        translation+=' '
        print(translation)

      
      

      #name transileration
      elif 'ner' in lst :
        if lst['word'] not in noun:
          if '<genitive>' in tuple(lst['morph'])[0]:
            out = e.translit_word(lst['word'],lang_code="ml",topk=1)
            translation+=str(out[0])+"'s"
            translation+=' '
        else:
          if '<genitive>' in tuple(lst['morph'])[0]:
            # morph_gen.morph()
            translation+=noun.get(lst['word'])+"'s"
            translation+=' '
          else:
            translation+=noun.get(lst['word'])+' '

        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '

      elif lst['entity']=='V_VM_VINF' or '<v>' in tuple(lst['morph'])[0]:
        
        #simple present
        if '<present>' in tuple(lst['morph'])[0]:
          translation = simple_present(lst,translation)
          # translation+=verb_1.get(lst['word'])+'s'   


        #simple past
        elif '<past>' in tuple(lst['morph'])[0]:
          translation=simple_past(lst,translation)

        #simple future
        elif '<future>' in tuple(lst['morph'])[0]:
          translation=simple_future(lst,translation)  


        # Present continuous
        elif '<iterative-present-aspect>' in tuple(lst['morph'])[0]:
          translation=present_continous(lst,translation)     

        #past continuous
        elif lst['word'] in past_conjuntions:
          translation=past_continous(lst,translation) 


        # Present Perfect
        elif '<remote-perfect-aspect>' in tuple(lst['morph'])[0]:  

          if lst['word'] in conjunctions:
            continue
          translation+='have' if translation.strip() in present_auxilary_inflection.keys() else 'has'
          translation+=' '
          translation+=verb_3.get(lst['word'])

        # Past Perfect
        elif '<simple-perfect-aspect>' in tuple(lst['morph'])[0]:
          translation+=past_perfect(lst)

        elif '<aff>' in tuple(lst['morph'])[0] and lst['entity']=='V_VN_VINF':
          translation+=personal_pronoun(lst)
        
        #purposive mood(2 verb situation)
        elif '<purposive-mood>' in tuple(lst['morph'])[0]:
          translation+=verb_1.get(lst['word'])+' '
          translation+='to'
          translation+=' '

        else:
          print("Rules Breaked")
    final_translation=' '
    translation=translation.lstrip()
    word_list=list(translation.split(" "))
    word_list.remove('')
    print("word_type",word_type)
    print("word_list",word_list)
    if (list(word_type.values())[1]=='N_NN' or list(word_type.values())[1]=='N_NNP') and list(word_type.values())[2]!='JJ':
      if len(word_list)==3:
        word_list[1],word_list[2]=word_list[2],word_list[1]
        final_translation=' '.join(word_list)
      else:
        word_list[1],word_list[3]=word_list[3],word_list[1]
        final_translation=' '.join(word_list)
    elif list(word_type.values())[1]=='V_VM_VINF':
      if len(word_list)==3:
        word_list[1],word_list[2]=word_list[2],word_list[1]
        final_translation=' '.join(word_list)
      else:
        word_list[1],word_list[3]=word_list[3],word_list[1]
        final_translation=' '.join(word_list)
    elif list(word_type.values())[1]=='PR_PRP':
      if len(word_list)==3:
        word_list[1],word_list[2]=word_list[2],word_list[1]
        final_translation=' '.join(word_list)
      else:
        word_list[1],word_list[3]=word_list[3],word_list[1]
        final_translation=' '.join(word_list)
    elif list(word_type.values())[2]=='JJ':
      print(word_list)
      if len(word_list)==3:
        word_list[1],word_list[2]=word_list[2],word_list[1]
        final_translation=' '.join(word_list)
      else:
        word_list[2],word_list[3]=word_list[3],word_list[2]
        final_translation=' '.join(word_list)
        print("translation",final_translation)
    else:
      result=translation  
    result=final_translation
  elif (len(representation_list) == 5):
    translation=' '
    word_type={}
    for lst in representation_list:

      word_type.update({lst['word']:lst['entity']})

      if (lst['entity']=='N_NNP' or lst['entity']=='N_NN') and 'ner' not in lst:


        if '<n>' in tuple(lst['morph'])[0]:
          translation+=noun_presence(lst)

        else:
          translation+=noun.get(lst['word'])+' '
        
        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '

      elif lst['entity']=='CC_CCD':
        if 'and' not in translation:
          translation+='and'
          translation+=' '

      elif lst['entity']=="PR_PRP":
        translation=proper_pronoun(lst,translation)

      elif lst['entity']=='DM_DMD' and 'ner' not in lst:
        translation=demonstrative.get(lst['word'])+' '

      #name transileration
      elif 'ner' in lst :
        if lst['word'] not in noun:
          out = e.translit_word(lst['word'],lang_code="ml",topk=1)
          translation+=str(out[0])+' '
        else:
          translation+=noun.get(lst['word'])+' '

        for prepos in prepositions.keys():
          if prepos in tuple(lst['morph'])[0]:
            translation+=prepositions[prepos]+' '

      elif lst['entity']=='V_VM_VINF' or '<v>' in tuple(lst['morph'])[0]:
        
        #simple present
        if '<present>' in tuple(lst['morph'])[0]:
          translation+=verb_1.get(lst['word'])+'s'   


        #simple past
        elif '<past>' in tuple(lst['morph'])[0]:
          translation=simple_past(lst,translation)

        #simple future
        elif '<future>' in tuple(lst['morph'])[0]:
          translation=simple_future(lst,translation)  


        # Present continuous
        elif '<iterative-present-aspect>' in tuple(lst['morph'])[0]:
          translation=present_continous(lst,translation)     

        #past continuous
        elif lst['word'] in past_conjuntions:
          translation=past_continous(lst,translation) 


        # Present Perfect
        elif '<remote-perfect-aspect>' in tuple(lst['morph'])[0]:  

          if lst['word'] in conjunctions:
            continue
          translation+='have' if translation.strip() in present_auxilary_inflection.keys() else 'has'
          translation+=' '
          translation+=verb_3.get(lst['word'])

        # Past Perfect
        elif '<simple-perfect-aspect>' in tuple(lst['morph'])[0]:
          translation+=past_perfect(lst)

        elif '<aff>' in tuple(lst['morph'])[0] and lst['entity']=='V_VN_VINF':
          translation+=personal_pronoun(lst)
        
        #purposive mood(2 verb situation)
        elif '<purposive-mood>' in tuple(lst['morph'])[0]:
          translation+=verb_1.get(lst['word'])+' '
          translation+='to'
          translation+=' '

        else:
          print("Rules Breaked")
    result=translation
  return translation,representation_list



# സീത ആനയെ നോക്കി
if __name__ == "__main__":
  sentence = ""
  while(sentence != "q"):
    sentence = input("Enter The Sentence: ")
    translation, representation_list = translator(sentence)
    print("Translation: ", translation)
    print("Representation List: ", representation_list)