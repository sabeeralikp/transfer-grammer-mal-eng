import streamlit as st
from main import translator
import pandas as pd
#from test import *

st.title("Malayalam - English Transfer Grammar Tool")

# df=pd.read_excel('Transformer rules Malayalam to English (2).xlsx')

# column_data=df['Malayalam Sentences']
# for i in colu:


sentence = st.text_input('Malayalam Sentence')

if st.button("Translate"):
    output,table = translator(sentence)
    st.success(output)
    df=pd.DataFrame.from_dict(table).astype('str')
    st.table(df)