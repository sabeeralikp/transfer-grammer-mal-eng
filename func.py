from data_read import *
from main import *



#conjunction
def check_two_word_conjuction(representation_list):

  if len(representation_list) == 3:

    for conjuction in conjunctions:

      if representation_list[2]['word'] == conjuction:
        return True
      
  return False

#inflection checking
def inflection(inflection_dict,verb_1,lst):

  for inflection_character_src, inflection_character_tgt in inflection_dict.items():
    inflection_verb_to_replace_val=''
    inflection_verb_replace_val=''
    print(inflection_verb_replace_val)
    print(inflection_verb_replace_val)

    if verb_1.get(lst['word']).endswith(inflection_character_src):
      inflection_verb_to_replace_val=inflection_character_src
      inflection_verb_replace_val=inflection_character_tgt
      break

  return inflection_verb_to_replace_val,inflection_verb_replace_val


def cons(word,vowels):

  if len(word)==3 or len(word)==4:

    if word[-1] not in vowels and word[-2] in vowels and word[-3] not in vowels and word[-1] not in exception:

      return word[-1]
    
#proper_pronoun
def proper_pronoun(lst,translation):

  if tuple(lst['morph'])[0] in plural_pronouns:
    translation+=plural_pronouns.get(tuple(lst['morph'])[0])+" "

  elif '<genitive>' in tuple(lst['morph'])[0]:
    translation+=possessive_pronouns.get(lst['word'])+' '

  else: 
    translation+=singular_pronouns.get(lst['word'])+" "

  return translation

#simple_present
def simple_present(lst,translation,ner_store):

  inflection_verb_to_replace = ''
  inflection_verb_replace = ''
  print("word",translation.strip())
  x=translation.split()
  if x[0] in third_person_pronoun or ner_store!='':


    if (verb_1.get(lst['word'])[-2]) not in vowel:
      print(True)
      inflection_verb_to_replace,inflection_verb_replace=inflection(inflection_simple_present_words,verb_1,lst)

      if inflection_verb_replace!='':
        translation+=verb_1.get(lst['word']).replace(inflection_verb_to_replace,inflection_verb_replace)

      else:
        translation+=verb_1.get(lst['word'])
        translation+='s'

    else:
      print(False)
      translation+=verb_1.get(lst['word'])
      translation+='s'

  else:
    translation+=verb_1.get(lst['word'])

  return translation

#simple past
def simple_past(lst,translation):

  if verb_1.get(lst['word']) in irregular_verb:
    word=verb_1.get(lst['word'])
    translation+=irregular_verb.get(word)

  else:
    translation+=verb_1.get(lst['word'])

    if verb_1.get(lst['word']).endswith('e'):
      translation+='d'

    # elif verb_1.get(lst['word'])[-1] not in vowel and verb_1.get(lst['word'])[-2] in vowel and verb_1.get(lst['word'])[-1] not in exception:
    #   translation+=verb_1.get(lst['word'])[-1]+'ed'

    elif verb_1.get(lst['word'])[-2] not in vowel and verb_1.get(lst['word']).endswith('y'):
      translation=translation[:-1]
      translation+='ied'

    else:
      translation += 'ed'
      translation += ' '

  return translation


#simple fututre
def simple_future(lst,translation):

  translation+='will'
  translation+=' '
  translation+=verb_1.get(lst['word'])

  return translation

#present continous
def present_continous(lst,translation):

  inflection_verb_to_replace = ''
  inflection_verb_replace = ''

  if translation.strip() in present_auxilary_inflection.keys():
    translation += 'am' if translation.strip() == 'i' else 'are'

  else:
    translation += 'is'

  translation+=' '
  inflection_verb_to_replace,inflection_verb_replace=inflection(inflection_continuous_words,verb_1,lst)

  if inflection_verb_replace!='':
    add_word=verb_1.get(lst['word'])
    translation+=add_word[:-1]+inflection_verb_replace

  else:
    translation += verb_1.get(lst['word'])
  res=cons(verb_1.get(lst['word']),vowel)

  if res!=None:
    translation+=res

  if 'ing' not in translation:
    translation+='ing'
  return translation

#past continous
def past_continous(lst,translation):

  translation+='was' if translation.strip() in singular else 'were'
  translation += ' '
  inflection_verb_to_replace = ''
  inflection_verb_replace = ''

  for inflection_character_src, inflection_character_tgt in inflection_continuous_words.items():

    if verb_1.get(lst['morph'][0].split('<v>')[0]).endswith(inflection_character_src):
      inflection_verb_to_replace = inflection_character_src
      inflection_verb_replace = inflection_character_tgt

  if inflection_verb_replace != '':
    translation += verb_1.get(lst['morph'][0].split('<v>')[0]).replace(inflection_verb_to_replace, inflection_verb_replace)

  else:
    translation += verb_1.get(lst['morph'][0].split('<v>')[0])
    res=cons(verb_1.get(representation_list[1]['word']),vowel)

    if res!=None:
      translation+=res

    if 'ing' not in translation:
      translation+='ing'

  return translation

#past perfect
def past_perfect(lst,translation):

  translation = ''
  translation+='had'
  translation+=' '
  translation+=verb_3.get(lst['word'])

  return translation

#personal pronoun
def personal_pronoun(lst):

  if representation_list[0]['word'] in first_person_pronoun.keys():
    translation+='am'

  elif representation_list[0]['word'] in generic_personal_pronouns.keys():
    translation+='are'

  elif representation_list[0]['word'] in third_person_singular_pronoun.keys():
    translation+='is'

  translation+=' '
  new_word=verb_1.get(lst['word'])
  translation+=new_word[:-1]
  translation+='ing'

  return translation

#check noun presence
def noun_presence(lst):
  translation=''

  if '<n><pl>' in tuple(lst['morph'])[0]:

    if lst['word'] in noun_plural_exceptions:
      translation+=noun_plural_exceptions.get(lst['word'])+' '

    else:
      translation+=noun.get(lst['word'])+'s'
      translation+=' '

  else:
    translation+=noun.get(lst['word'])+' '

  return translation


#name entity conversion
def name_entity(lst, e):

  translation=''
  out = e.translit_word(lst['word'],lang_code="ml",topk=1)
  translation+=str(out[0])+' '

  return translation

def prepositions_check(lst):
  translation=''
  for prepos in prepositions.keys():
    if prepos in tuple(lst['morph'])[0]:
      translation+=prepositions[prepos]+' '
  return translation
