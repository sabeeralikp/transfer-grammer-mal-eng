import json

#read json files
def load_resource(path):

  with open(path,"r",encoding="utf8") as f:
    res=json.loads(f.read())
  return res
  

#load json files 
affix=load_resource("resources/affix.json")
an=load_resource("resources/an.json")
conjunctions=load_resource("resources/conjunctions.json")
demonstrative=load_resource("resources/demonstrative.json")
exception=load_resource("resources/exception.json")
first_person_pronoun=load_resource("resources/first_person_pronoun.json")
generic_personal_pronouns=load_resource("resources/generic_personal_pronouns.json")
inflection_continuous_words=load_resource("resources/inflection_continuous_words.json")
inflection_simple_present_words=load_resource("resources/inflection_simple_present_words.json")
irregular_verb=load_resource("resources/irregular_verb.json")
present_auxilary_inflection=load_resource("resources/present_auxilary_inflection.json")
singular_pronouns=load_resource("resources/singular_pronouns.json")
singular=load_resource("resources/singular.json")
third_person_pronoun=load_resource("resources/third_person_pronoun.json")
noun_plural_exceptions=load_resource("resources/noun_plural_exceptions.json")
noun=load_resource("resources/noun.json")
past_conjuntions=load_resource("resources/past_conjuntions.json")
plural_pronouns=load_resource("resources/plural_pronouns.json")
plural=load_resource("resources/plural.json")
third_person_singular_pronoun=load_resource("resources/third_person_singular_pronoun.json")
verb_1=load_resource("resources/verb_1.json")
verb_2=load_resource("resources/verb_2.json")
verb_3=load_resource("resources/verb_3.json")
vowel=load_resource("resources/vowel.json")
prepositions=load_resource("resources/prepositions.json")
possessive_pronouns=load_resource("resources/possessive_pronouns.json")